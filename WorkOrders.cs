﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

//-------
// Version: 1.3.0
//	If there is no directory defined, it will not ask for the Work Order number.
// Version: 1.3.1
//  Made OK button green.

namespace ShopFloorSystem
{
    //---------------------
    // Test data for shop floor system
    public struct shopfloordata
    {
        public string workorder;
        public string pn;
        public string user;
        public string topsn;
        public string pcbasn;
        public string mac;
        public DateTime startTestTime;
        public string station;
        public string testStatus;
        public string errorCode;
    }

    
    
    public class ShopFloorInterface
    {
        string lclFileDirectory = string.Empty;
        string lclFileName = string.Empty;
        shopfloordata sfData;
        int shopFiletype = 0;       // 0 - single file per run, 1 - one file for all runs.

        public string WorkOrderNumber { get { return sfData.workorder; } }
        public string PN { set { sfData.pn = value; } }
        public string TopSN { set { sfData.topsn = value; } }
        public string MAC { set { sfData.mac = value; } }
        public string PcbaSN { set { sfData.pcbasn = value; } }
        public DateTime StartTime { set { sfData.startTestTime = value; } }
        public string TestStatus { set { sfData.testStatus = value; } }
        public string ErrorCode { set { sfData.errorCode = value; } }

        public ShopFloorInterface(string filedirectory, string filename, string stationname, string user)
        {
            sfData = new shopfloordata();
            lclFileDirectory = filedirectory;
            lclFileName = filename;

            sfData.station = stationname;
            sfData.user = user;
            sfData.workorder = "";

            // define the file output. If a file is defined, all the data will go into it.
            //  if no file is defined, each SN will get is own file.
            if (filename == "")         // if no file is defined
                shopFiletype = 0;       // then single file per run
            else
                shopFiletype = 1;       // one file for all results

            // Verify that the directory exists
            if (!Directory.Exists(lclFileDirectory))
            {
                return;
            }

            GetWorkOrder getwo = new GetWorkOrder();
            getwo.ShowDialog();
            sfData.workorder = getwo.WorkOrderNumber;
        }

        /// <summary>
        /// This will generate the result file. If shopFileType is 1, then the data will be added
        /// to the directory/file name that was setup.
        /// If the shopFileType = 0, then each run will get its own file name.
        /// Format:
        ///     [serial number].txt
        /// </summary>
        public void RecordShopData()
        {
            string shopDataLine = string.Empty;
            string fullpathAndName = string.Empty;
            string singleFileName = string.Empty;

            if (Directory.Exists(lclFileDirectory))
            {
                if (shopFiletype == 1)      // if one file for all
                    fullpathAndName = Path.Combine(lclFileDirectory, lclFileName);
                else                        // each run gets a unique file name
                {
                    fullpathAndName = Path.Combine(lclFileDirectory, (sfData.pcbasn + ".txt"));
                }

                if (sfData.topsn == "")
                    shopDataLine = string.Format("TTime={0};PCBA_SN={1};MacSN={2};WorkOrder={3};Result={4};ErrorCode={5};Station={6};Operator={7}",
                                                    sfData.startTestTime.ToString(@"yyyy/MM/dd_HH:mm:ss"), sfData.pcbasn, sfData.mac.ToUpper(), sfData.workorder, sfData.testStatus,
                                                    sfData.errorCode, sfData.station, sfData.user);
                else
                    shopDataLine = string.Format("TTime={0};UnitSN={1};PCBA_SN={2};MacSN={3};WorkOrder={4};Result={5};ErrorCode={6};Station={7};Operator={8}",
                                                    sfData.startTestTime.ToString(@"yyyy/MM/dd_HH:mm:ss"), sfData.topsn, sfData.pcbasn, sfData.mac.ToUpper(), sfData.workorder, sfData.testStatus,
                                                    sfData.errorCode, sfData.station, sfData.user);
                using (StreamWriter writer = new StreamWriter(fullpathAndName, true))
                {
                    writer.WriteLine(shopDataLine);
                }
            }
        }
    }
}
